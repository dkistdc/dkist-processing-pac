Overview
========

The `dkist-processing-pac` code repository contains the polarization, analysis and calibration
(PA&C) library.  Primarily used by the DKIST automated processing pipelines, the PA&C library
computes telescope Mueller Matrices and instrument demodulation matrices.
